DISTRIBUTION := stretch

CHANGES_PATH :=
CHANGES_PATH += ../source/freetype
CHANGES_PATH += ../source/fontconfig
CHANGES_PATH += ../source/mupdf

upload:
	@for p in ${CHANGES_PATH}; do \
		for c in $$p/*.changes; do \
			if ! grep -q -e '^Distribution:\s*${DISTRIBUTION}' $$c; then \
				sed -i -e '/^Distribution:\s*.*/s//Distribution: ${DISTRIBUTION}/' $$c; \
			fi; \
			reprepro include stretch $$c; \
		done; \
	done
	@find pool -type f \( -iname '*.deb' -o -iname '*.udeb' \) | while read link; do \
		deb="$${link##*/}"; \
		for p in ${CHANGES_PATH}; do \
			if [ -e "$$p/$$deb" ]; then \
				ln -srf "$$p/$$deb" "$$link"; \
				break; \
			fi; \
		done; \
	done

remove:
	@packages=$$(find ${CHANGES_PATH} -type f \( -iname '*.deb' -o -iname '*.udeb' \) | sed -e 's,.*/,,' -e 's,_.*,,'); \
	reprepro remove ${DISTRIBUTION} $$packages

.PHONY: upload remove
