### Usage
```sh
cat <<'EOF' | sudo tee /etc/apt/sources.list.d/temprepo.list >/dev/null
deb [ trusted=yes ] file:///path/to/this/folder/ stretch main non-free contrib
EOF
sudo apt-get update
```
